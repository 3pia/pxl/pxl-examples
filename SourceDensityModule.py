### Creates Isotropic distributed sources with equal luminosity from a
### given Source density
# created by Tobias Winchen, 10.09.10 

from math import *
from pxl import modules
from pxl import core, astro

class SourceDensityModule(modules.PythonModule):
    def __init__(self):
        ''' Initialize private variables '''
        modules.PythonModule.__init__(self)
        self.counter = 0
        self.__sourceDensity = 0
        self.__maxDistance = 0
        self.__randomSeed = 0

    def initialize(self, module):
        ''' Initialize module options '''
        self.__module = module
        module.addOption("Source Density","Sources per Mpc**3", 1E-5)
        module.addOption("Max. Distance","Sphere radius to distribute the sources into, in Mpc", 1000)
        module.addOption("RandomSeed","Random seed to use, 0 for automatic", 0)

    def beginJob(self, parameters=None):
        '''Executed before the first object comes in'''
        self.__sourceDensity = self.__module.getOption("Source Density")
        self.__maxDistance = self.__module.getOption("Max. Distance")
        self.__randomSeed = self.__module.getOption("RandomSeed")
        if self.__randomSeed == 0:
          self.__random = core.Random()
        else:
          self.__random = core.Random(self.__randomSeed)

    def beginRun(self):
        '''Executed before each run'''
        pass

    def analyse(self,object):
        '''Executed on every object'''
        bc = core.toBasicContainer(object)
        if self.__randomSeed != 0:
          bc.setUserRecord("SourceDensityModule_RandomSeed", self.__randomSeed)

        numOfSources = int(self.__sourceDensity*4./3.*pi*self.__maxDistance**3)
        bc.setUserRecord("SourceDensity",self.__sourceDensity)
        bc.setUserRecord("Max. Source Distance",self.__maxDistance)
        for i in range(numOfSources):
          s = astro.UHECRSource(self.__random.randUnitVectorOnSphere())
          s.setName("Source_%i" % (i))
          s.setDistance((self.__random.randUniform(0,1))**(1./3) *
              self.__maxDistance)
          s.setLuminosity(1)
          bc.setObject(s)

    def endRun(self):
        '''Executed after each run'''
        pass

    def endJob(self):
        '''Executed after the last object'''
        print '*** End job'
