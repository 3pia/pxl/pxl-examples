### Skeleton for analysismodule.py
# created by VISPA
# Thu Jan 27 12:57:45 2011
### PyAnalyse skeleton script

#to make sure the analysis doesn't crash when container has a 
#healpix object
from pxl import modules, healpix
healpix.Healpix_initialize()
import pxl
#from pylab import *
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy

class Example(modules.PythonModule):
    def __init__(self):
        ''' Initialize private variables '''
        modules.PythonModule.__init__(self)
        self._countBC = 0
        #self._exampleVariable = startValue
        
    def initialize(self, module):
        ''' Initialize module options '''
        module.addOption("SaveFigureToFile", "Save the figure to the specified filename", "skyplot.png")
        module.addOption("minimalEnergy", "log10(minimal Energy)", 18)
        module.addOption("maximalEnergy", "log10(maximal Energy)", 20)
        self.__module = module
    
    def beginJob(self, parameters=None):
        '''Executed before the first object comes in'''
        print '*** Begin job'
        self.__fileName = self.__module.getAnalysis().getOutputPath() +  self.__module.getOption("SaveFigureToFile")
        self.__minimalEnergy = self.__module.getOption("minimalEnergy")
        self.__maximalEnergy = self.__module.getOption("maximalEnergy")
    
    def beginRun(self):
        '''Executed before each run'''
        pass
    
    def analyse(self, object):
        '''Executed on every object'''
    	bc = core.toBasicContainer(object)
    	crs = bc.getObjectsOfType(pxl.astro.UHECR)

        fig_= plt.figure()
        plt.subplot(111, projection='hammer')
        lons = []
        lats = []
        energies = []
        

        for i,a in enumerate(crs):
            energy = numpy.log10(a.getEnergy())+18
            if(energy >= self.__minimalEnergy and energy <= self.__maximalEnergy):
                lons.append(a.getGalacticLongitude())
                lats.append(a.getGalacticLatitude())
                energies.append(energy)

        lons = numpy.array(lons)
        lats = numpy.array(lats)
        energies = numpy.array(energies)
        indices = numpy.argsort(energies)
        lons = lons[indices]
        lats = lats[indices]
        energies = energies[indices]
        plt.scatter(lons, lats, c=energies, vmin=self.__minimalEnergy-0.5, vmax=self.__maximalEnergy+0.5)
        
        if(len(energies)>0):
            plt.title("Cosmic Rays")
            co = plt.colorbar(orientation='horizontal')
            co.set_label("log10(Energy [EeV])")

        
        if self.__fileName == "":
            print "Now Filename given to save the figure."
        else:
            print "Creating outputfile: %s"%self.__fileName
            plt.savefig(self.__fileName)
        self._countBC += 1
	
    def endRun(self):
        '''Executed after each run'''
        pass
    
    def endJob(self):
        '''Executed after the last object'''
        print '*** End job'
