### loadAugerPublicData.py
# created by Tobias Winchen 
# Mon Sep 26 17:40:56 2011
#
# This generator module loads the publically available data of the
# Pierre Auger Observatory from http://auger.colostate.edu/ED/
# and passes the events to the output either individually or as complete
# dataset in a BasicContainer.


from pxl import modules, astro
from math import pi
import urllib
import os

class PublicAugerDataModule(modules.PythonModule):
    def __init__(self):
        ''' Initialize private variables '''
        modules.PythonModule.__init__(self)
        #self._exampleVariable = startValue

    def initialize(self, module):
        ''' Initialize module options '''
        module.addOption("IndividualEvents", "If checked, pass down data as individual events. If false pass down dataset in single BasicContainer. Default false", True)
        module.addOption("DataSource", "Url to public auger data or file on disk", 'http://auger.colostate.edu/ED/_data.php?dos=1')
        self.__module = module
        #module.addOption("example option", "example description", "default value")

    def beginJob(self, parameters=None):
        '''Executed before the first object comes in'''
        print '*** PublicAugerDataModule: Begin job'

        self.__individualEvents = self.__module.getOption('IndividualEvents')
        self.__counter = 0
        dataSource =  self.__module.getOption('DataSource')
        print '  * Fetching data from:', dataSource, '...'
        try:
          if dataSource.startswith('http://'):
            self.__url = urllib.urlopen(dataSource)
          else:
            self.__url = open(os.path.join(os.path.dirname(os.path.realpath(__file__)), dataSource))
          txtData = [l for l in self.__url.readlines() if len(l) > 0 and not l.startswith('#')]
        except:
          errorMessage = "Error reading data from %s ! Aborting ..." % (dataSource)
          raise RuntimeError(errorMessage)

        self.__data = []
        erLn = 0

        print '  * Parsing data ...'
        for line in txtData:
          l = line.split()
          if len(l) != 8:
            erLn+=1
            continue
          try:
            cr = astro.UHECR()
            cr.setUserRecord('AugerEventId', l[0])
            cr.setUserRecord('NumberOfStationsInEvent', int(l[1]))
            cr.setUserRecord('ThetaReconstructed', float(l[2]))
            cr.setUserRecord('PhiReconstructed', float(l[3]))
            cr.setEnergy(float(l[4]))
            cr.setTime(int(l[5]))
            cr.setGalacticCoordinates(float(l[6])/180*pi, float(l[7])/180*pi)
            self.__data.append(cr)
          except:
            erLn+=1
        print '  * Done reading data. '
        if erLn > 0:
            'Error in reading %i / %i lines.' % (erLn, len(self.__data))

    def beginRun(self):
        '''Executed before each run'''
        pass

    def generate(self):
        '''Executed on every object'''
        if self.__individualEvents:
          if self.__counter < len(self.__data):
            self.__counter+=1
            return self.__data[self.__counter-1]
          else:
            return None
        elif self.__counter == 0:
          self.__b = core.BasicContainer()
          self.__counter+=1
          for cr in self.__data:
            self.__b.insertObject(cr)
          return self.__b
        return None


    def endRun(self):
        '''Executed after each run'''
        pass

    def endJob(self):
        '''Executed after the last object'''
        print '*** PublicAugerDataModule: End job'
