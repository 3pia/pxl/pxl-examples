import math

from pxl import modules

''' Create particles from parsed CMS data '''
class CreateParticles(modules.PythonModule):
    def __init__(self):
        ''' Initialize private variables '''
        modules.PythonModule.__init__(self)
        #self._exampleVariable = startValue

    def initialize(self, module):
        ''' Initialize module options '''
        #module.addOption("example option", "example description", "default value")

    def beginJob(self, parameters=None):
        '''Executed before the first object comes in'''
        print '*** Begin job: Create particles for CMS public data'

    def beginRun(self):
        '''Executed before each run'''
        pass

    def analyse(self, object):
        '''Executed on every object'''
        event = core.toEvent(object)
        
        
        if event.hasUserRecord("px1"):
            # Create both leptons for lepton-lepton data
            lepton1 = event.createParticle()
            lepton1.setName("lepton1")
            px1 = event.getUserRecord("px1")
            py1 = event.getUserRecord("py1")
            pz1 = event.getUserRecord("pz1")
            e1 = event.getUserRecord("E1")
            lepton1.setP4(px1, py1, pz1, e1)
        
            lepton2 = event.createParticle()
            lepton2.setName("lepton2")
            px2 = event.getUserRecord("px2")
            py2 = event.getUserRecord("py2")
            pz2 = event.getUserRecord("pz2")
            e2 = event.getUserRecord("E2")
            lepton2.setP4(px2, py2, pz2, e2)

        elif event.hasUserRecord("MET"):
            # Create lepton and MET for lepton-MET data
            lepton = event.createParticle()
            lepton.setName("lepton")
            px = event.getUserRecord("px")
            py = event.getUserRecord("py")
            pz = event.getUserRecord("pz")
            e = event.getUserRecord("E")
            lepton.setP4(px, py, pz, e)
            
            met = event.createParticle()
            met.setName("MET")
            missinget = event.getUserRecord("MET")
            missingphi = event.getUserRecord("phiMET")
            
            # Calculate px/py components of missing ET
            missingpx = missinget * math.cos(missingphi)
            missingpy = missinget * math.sin(missingphi)
            missingpz = 0. # No information about the z component
            
            # missing ET is equivalent to total energy
            met.setP4(missingpx, missingpy, missingpz, missinget)
            
            

    def endRun(self):
        '''Executed after each run'''
        pass

    def endJob(self):
        '''Executed after the last object'''
        print '*** End job: Create particles for CMS public data'
