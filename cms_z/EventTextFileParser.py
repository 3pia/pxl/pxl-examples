### Skeleton for newestParser.py
# created by VISPA
# Mon Aug 30 20:28:46 2010
### PyGenerate skeleton script

from pxl import modules
import sys

class EventTextFileParser(modules.PythonModule):
    def __init__(self):
        ''' Initialize private variables '''
        modules.PythonModule.__init__(self)

        self._moduleRef = None
        
        # 'Global' variables
        self._nEvents = 0
        self._inFile = 0
        self._whiteSpaceSeperator = False

        # Options to be added in initialize!
        self._textFileName = ""
        self._separator = ""
        self._comment = ""
        self._skipLines = 0
        self._maxEntry = 0
        self._varTitles = True
        self._varTitleList = []
        
        pass


    def initialize(self, module):
        ''' Initialize module options '''
        print "--- Init FileParser"

        module.addOption("textFileName", "Name of data input file", "", modules.OptionDescription().USAGE_FILE_OPEN)
        module.addOption("separator", "How are columns seperated in the text file", " ")
        module.addOption("comment", "What is the exit character for comments", "#")
        module.addOption("skipLines", "Number of rows to ignore at beginning of file", 0)
        module.addOption("maxEntry", "Last entry to be considered, -1 means consider all. Count begins with first data event.", -1)
        module.addOption("varTitles", "Whether the first line after the comments contains the variable titles", True)
        module.addOption("varTitleList", "Name the variables corresponding to the passed list. If this list is not empty, the names in the file are ignored.", [])

        self._moduleRef = module

    def beginJob(self, parameters=None):
        '''Executed before the first object comes in'''

        self._textFileName = self._moduleRef.getOption('textFileName')
        self._textFileName = self._moduleRef.getAnalysis().findFile(self._textFileName)
        print '*** Begin job: File Parser', self._textFileName

        self._separator = self._moduleRef.getOption('separator')
        self._comment = self._moduleRef.getOption("comment")
        self._skipLines = self._moduleRef.getOption('skipLines')
        self._maxEntry = self._moduleRef.getOption('maxEntry')

        if self._separator == "":
            print "ERROR - please specify separator! It will be set to ' ' (space)."
            self._separator = " "

        if self._separator == " ":
            self._whiteSpaceSeperator = True

        self._varTitles = self._moduleRef.getOption('varTitles')
        
        self._varTitleList = self._moduleRef.getOption('varTitleList')
        if len(self._varTitleList) > 0:
            print "Using the following title list", self._varTitleList
            self._varTitles = True
        
        self._inFile = open(self._textFileName, 'r')
        
        
        # skip the first lines = entries
        for i in xrange(self._skipLines):
            l = self._inFile.readline()
        if self._skipLines > 0:
            print "Skipped", self._skipLines, "lines"
            
        # Read variable titles
        if self._varTitles and len(self._varTitleList) == 0:
            line = self._inFile.readline()
            self._varTitleList = self.cleanSplit(line)
        elif len(self._varTitleList) == 0:
            print "No variable titles set. Using unnamed variables"
        else:
            self._varTitles = True
        
        
        print "Variable titles:", self._varTitleList


    def beginRun(self):
        '''Executed before each run'''
        pass

    def generate(self):
        '''Executed on every object'''
        
        if self._maxEntry > 0 and self._nEvents >= self._maxEntry:
            return None
        
        event = core.Event()

        line = self._inFile.readline()
        splitLine = self.cleanSplit(line)
        
        if splitLine == []:
            return None
        
        if len(self._varTitleList) != len(splitLine):
            print "WARNING, length of variable title list not equal to length of variable list"
        
        for i, entry in enumerate(splitLine):
            # Convert entry to float or int, if possible
            if '.' in entry:
                try:
                    entry = float(entry)
                except ValueError:
                    pass
            elif entry.isdigit():
                entry = int(entry)
            
            if self._varTitles and len(self._varTitleList) >= i:
                event.setUserRecord(self._varTitleList[i], entry)
            else:
                event.setUserRecord(str(i), entry)

        self._nEvents += 1
        return event

    def endRun(self):
        '''Executed after each run'''
        pass

    def endJob(self):
        '''Executed after the last object'''
        self._inFile.close()
        print '*** End job: TextFileParser', self._textFileName

 

    def cleanSplit(self, line):
        # get rid of comments and leading / trailing spaces...
        line = line.split(self._comment)[0].strip()
        if line:
            line = line.split(self._separator)
            cleanLine = []
            # Get rid of whitespace in each column
            if self._whiteSpaceSeperator:
                newLine = []
                for lineEle in line:
                    for part in lineEle.split("\t"):
                        newLine.append(part)
                line = newLine
                for object in line:
                    if object != '': cleanLine.append(object)
            else:
                for object in line:
                    cleanLine.append(object.strip())
            return cleanLine
        else:
            return []