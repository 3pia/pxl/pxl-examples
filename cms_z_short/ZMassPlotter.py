# Purpose: Reconstruct Z boson from two leptons and plot invariant mass

import ROOT
from ROOT import gROOT, gStyle

from pxl import modules

try:
    from pxl import root
    rootEnabled = True
except:
    print "Using PXL version < 3.3 or PXL compiled without ROOT"
    rootEnabled = False

class ZMassPlotter(modules.PythonModule):
    #----------------------------------------------------------------------
    def initialize(self, module):
        # ROOT histogram style
        
        # Requires PXL version >= 3.3
        if rootEnabled:
            root.PxlStyle.initRootPlotStyle()
        else:
            self.initPlotStyle()

        self._module = module
        
    def beginJob(self, parameters=None):
        print "*** Begin Job: Z mass plotter"

        # Book Z mass histogram
        self._h_rec_Zmass = ROOT.TH1F ("h_rec_Zmass", "", 40, 0., 120.)
        self._h_rec_Zmass.UseCurrentStyle()
        self._h_rec_Zmass.GetXaxis().SetTitle("Mass [GeV]")
        self._h_rec_Zmass.GetYaxis().SetTitle("Events")

    # Main event loop
    def analyse(self, object):
        # only process pxl::Events
        event = core.toEvent(object)
        if not event:
            return

        # Look for two leptons in the event
        lepton1 = 0
        lepton2 = 0
        
        particles = event.getParticles()
        
        for particle in particles:
            if particle.getName() == "lepton1":
                lepton1 = particle
            elif particle.getName() == "lepton2":
                lepton2 = particle
        
        if lepton1 and lepton2:
            # Create Z particle
            Z = event.createParticle()
            Z.setName('Z')
            
            # Reconstruct Z boson from two leptons
            Z.linkDaughter(lepton1)
            Z.linkDaughter(lepton2)
            Z.setP4FromDaughters()
            
            # Fill histogram
            self._h_rec_Zmass.Fill(Z.getMass())

    def endJob(self):
        print "*** End job: Z mass plotter"

        c1 = ROOT.TCanvas('c1', 'muon', 600, 600)
        
        f1 = ROOT.TF1("gauss", "gaus", 80., 100.)
        f1.SetParameter(1, 90.)
        self._h_rec_Zmass.Fit(f1, '', '', 80., 100.)

        self._h_rec_Zmass.DrawCopy("e")
        c1.Print(self._module.getAnalysis().getOutputFile("output/zmass.pdf"))
        #c1.Print(self._module.getAnalysis().getOutputFile("output/zmass.root"))


    def initPlotStyle(self):
        gROOT.SetStyle("Plain")
        gStyle.SetOptFit()
        gStyle.SetOptStat(0)

        # For the canvas:
        gStyle.SetCanvasBorderMode(0)
        gStyle.SetCanvasColor(ROOT.TStyle.kWhite)
        gStyle.SetCanvasDefH(600) #Height of canvas
        gStyle.SetCanvasDefW(600) #Width of canvas
        gStyle.SetCanvasDefX(0)   #POsition on screen
        gStyle.SetCanvasDefY(0)

        # For the Pad:
        gStyle.SetPadBorderMode(0)
        # gStyle.SetPadBorderSize(Width_t size = 1)
        gStyle.SetPadColor(ROOT.TStyle.kWhite)
        gStyle.SetPadGridX(False)
        gStyle.SetPadGridY(False)
        gStyle.SetGridColor(0)
        gStyle.SetGridStyle(3)
        gStyle.SetGridWidth(1)

        # For the frame:
        gStyle.SetFrameBorderMode(0)
        gStyle.SetFrameBorderSize(1)
        gStyle.SetFrameFillColor(0)
        gStyle.SetFrameFillStyle(0)
        gStyle.SetFrameLineColor(1)
        gStyle.SetFrameLineStyle(1)
        gStyle.SetFrameLineWidth(1)

        # For the histo:
        # gStyle.SetHistFillColor(1)
        # gStyle.SetHistFillStyle(0)
        gStyle.SetHistLineColor(1)
        gStyle.SetHistLineStyle(0)
        gStyle.SetHistLineWidth(1)
        # gStyle.SetLegoInnerR(Float_t rad = 0.5)
        # gStyle.SetNumberContours(Int_t number = 20)

        gStyle.SetEndErrorSize(2)
        #gStyle.SetErrorMarker(20)
        gStyle.SetErrorX(0.)

        gStyle.SetMarkerStyle(20)
        #gStyle.SetMarkerStyle(20)

        #For the fit/function:
        gStyle.SetOptFit(1)
        gStyle.SetFitFormat("5.4g")
        gStyle.SetFuncColor(2)
        gStyle.SetFuncStyle(1)
        gStyle.SetFuncWidth(1)

        #For the date:
        gStyle.SetOptDate(0)
        # gStyle.SetDateX(Float_t x = 0.01)
        # gStyle.SetDateY(Float_t y = 0.01)

        # For the statistics box:
        gStyle.SetOptFile(0)
        gStyle.SetOptStat(0) # To display the mean and RMS:   SetOptStat("mr")
        gStyle.SetStatColor(ROOT.TStyle.kWhite)
        gStyle.SetStatFont(42)
        gStyle.SetStatFontSize(0.025)
        gStyle.SetStatTextColor(1)
        gStyle.SetStatFormat("6.4g")
        gStyle.SetStatBorderSize(1)
        gStyle.SetStatH(0.1)
        gStyle.SetStatW(0.15)

        # Margins:
        #gStyle.SetPadTopMargin(0.15)
        gStyle.SetPadTopMargin(0.05)
        #gStyle.SetPadBottomMargin(0.13)
        gStyle.SetPadLeftMargin(0.16)
        gStyle.SetPadRightMargin(0.04) # top group adaption, original is 0.02
        gStyle.SetPadBottomMargin(0.13)

        # For the Global title:

        gStyle.SetOptTitle(0)
        gStyle.SetTitleFont(42)
        gStyle.SetTitleColor(1)
        gStyle.SetTitleTextColor(1)
        gStyle.SetTitleFillColor(10)
        gStyle.SetTitleFontSize(0.05)

        # For the axis titles:
        gStyle.SetTitleColor(1, "XYZ")
        gStyle.SetTitleFont(42, "XYZ")
        #gStyle.SetTitleSize(0.06, "XYZ")
        gStyle.SetTitleSize(0.06, "XYZ")
        # gStyle.SetTitleXSize(Float_t size = 0.02) # Another way to set the size?
        # gStyle.SetTitleYsize(Float_t size = 0.02)
        gStyle.SetTitleXOffset(0.9)
        gStyle.SetTitleYOffset(1.25)
        #gStyle.SetTitleOffset(1.1, "Y") # Another way to set the Offset

        # For the axis labels:

        gStyle.SetLabelColor(1, "XYZ")
        gStyle.SetLabelFont(42, "XYZ")
        gStyle.SetLabelOffset(0.007, "XYZ")
        gStyle.SetLabelSize(0.05, "XYZ")
        #gStyle.SetLabelSize(0.04, "XYZ")

        # For the axis:

        gStyle.SetAxisColor(1, "XYZ")
        gStyle.SetStripDecimals(True)
        gStyle.SetTickLength(0.03, "XYZ")
        gStyle.SetNdivisions(510, "XYZ")
        gStyle.SetPadTickX(1)  # To get tick marks on the opposite side of the frame
        gStyle.SetPadTickY(1)

        # Change for log plots:
        gStyle.SetOptLogx(0)
        gStyle.SetOptLogy(0)
        gStyle.SetOptLogz(0)

        gStyle.SetPalette(1) #(1,0)

        # another top group addition
        gStyle.SetHatchesSpacing(1.0)

        # Postscript options:
        gStyle.SetPaperSize(20., 20.)
        
        print "Plot Style initialized"
