### GraphPlotter.py
# Purpose:  This script creates plots which contain a graph created
#           from pxl NVectrors. These vectors can for example be 
#           created with the TextFileParser Generator.
#           
# Author:   Joschka Lingemann
# Created:  Tue Jul 20 10:56:16 2010

# Import all necessary stuff from root:
from ROOT import gROOT, gStyle, TGraph, TGraphErrors, TF1, TCanvas
# This we'll need for simple platform independent
# manipulation of paths:
from os import path
# Arrays are used in the constructor of the TGraph
from array import array
# PXL stuff
from pxl import modules

class GraphPlotter(object):
    def __init__(self):
        '''
            Initialize the class, initialize private variables
        '''
        self._moduleRef = None

        self._variablePairs = []
        self._errorPairs = []
        self._unitPairs = []
        self._saveAs = ""
        self._optionsGood = False

        pass

    def initialize(self, module):
        """
            This is executed when you add the module or reload it.
            It adds the otions in the VISPA GUI
        """
        print "--- Init: GraphPlotter"

        # Here the options one can set in the VISPA GUI are added

        # The variable pairs will be plotted against each other.
        # For example x:y will result in a graph with x vs. y
        module.addOption("variablePairs", "Variables to plot against each other, variables seperated by colon pairs seperated by space e.g., px:py", "px:py px:pz")
        # The error pairs have to be denoted as the variables.
        # First error pair for first variable pair, second for 
        # the second variable pair, etc...
        # The first member of a pair will be used as the error
        # for the first member of a variable pair. Write a 0 
        # if a variable has no error.
        module.addOption("errorPairs", "Lines/Columns to use as errors for the variablePairs if no error should be used for one variable write 0 place holder e.g., 'errorx:errory' 'errorx:0' '0:errory'", "")

        # Unit pairs are again denoted as the variable pairs
        # and error pairs.
        # If a variable has no unit one can write a 0. The
        # variable will be added as an appendix to the axis 
        # title, e.g. if a variable pair is called x:y and the
        # unit pair is cm:0 it will result in the x-axis being
        # titled "x (cm)" and a y-axis with the title "y"
        module.addOption("unitPairs", "Units of the variables. Denote in same order with same syntax as variablePairs and errorPairs above.", "cm:cm 0:cm")

        # The saveAs option is for the output of this module.
        # Graphs will be saved in the given format and with the
        # given title enumerated in the order as they were put 
        # in the variable pair field.
        # E.g. the saveAs option graphPlot.pdf will result in 
        # plots being saved as pdfs with the titles graphPlot1.pdf,
        # graphPlot2.pdf, etc.
        module.addOption("saveAs", "Specify a format in which the plot should be saved. Possible filetypes: pdf, eps, ps, png, gif, jpg", "graphPlot.pdf", modules.OptionDescription().USAGE_FILE_SAVE)

        # Set this for later access of the options
        self._moduleRef = module

    def beginJob(self, parameters=None):
        '''
            Here all options are get and dumped in corresponding variables.
            Only edit this, if you know what you are doing.

        '''
        print '*** Begin job: GraphPlotter'

        # This bool is used to check wether the configuration is 
        # good enough to prevent crashes.
        self._optionsGood = True

        # Get variable pairs, which are separated by a whitespace
        varPairsRaw = self._moduleRef.getOption("variablePairs").split()
        # Now split them and fill the pairs into a list
        if not varPairsRaw:
            print "ERROR in GraphPlotter: Please specify variables to draw."
            self._optionsGood = False
        for item in varPairsRaw:
            if len(item.split(":")) != 2:
                raise NameError("ERROR in GraphPlotter: Please always give pairs of variables, e.g. 'x:y t:x'. Do not use quotes.")
            else:
                self._variablePairs.append((item.split(":")[0], item.split(":")[1]))

        # NOTE: Not happy with this, yet. Students will have to write variables
        # and errors in the right order...
        # First get error pairs
        errPairsRaw = self._moduleRef.getOption("errorPairs").split()
        # Now split and fill into list, also appending 0s for variables
        # without errors
        self._errorPairs = self.splitPairOption(errPairsRaw, "errorPairs")
        # Do the same for units:
        unitPairsRaw = self._moduleRef.getOption("unitPairs").split()
        self._unitPairs = self.splitPairOption(unitPairsRaw, "unitPairs")

        # Get the saveAs string in a form so we can append numbers
        # for multiple plots per module instance...
        saveAsRaw = self._moduleRef.getAnalysis().getOutputFile() + self._moduleRef.getOption("saveAs")
        if not saveAsRaw:
            raise NameError("ERROR in GraphPlotter: Please specify the saveAs option!")
        # pathHead will now contain the path to the folder where
        # we want to save, pathTail only the filename:
        pathHead, pathTail = path.split(saveAsRaw)
        # This loop takes care of filenames with more than one 
        # . in them, for example my.plot.pdf 
        for i, pathTailPart in enumerate(pathTail.split(".")):
            if i == 0:
                self._saveAs = path.join(pathHead, pathTailPart)
            elif i < len(pathTail.split(".")) - 1:
                self._saveAs += pathTailPart
            else:
                self._saveAs += "{filenumber}." + pathTailPart


    def analyse(self, object):
        '''
            Executed on every object:
            Think of this as part of your for-loop which would go over all
            the entries in your text file.
            The actual work is being done here. 

            Please ONLY EDIT the code outside the user hooks 
            only IF YOU KNOW WHAT YOU'RE DOING.
        '''

        # The NVectors are saved in so called containers, this converts the object
        #  which is handed from module to module into this original container. 
        container = core.toBasicContainer(object)
        fileCounter = 0

        # This will setup a standard style for all plots and the canvases on which
        # the plots are drawn.
        # Customization will be possible anyway, using the normal pyROOT syntax
        self.setupRoot()

        # If the module is not correctly configured skip the processing to 
        # prevent crashes
        if not self._optionsGood:
            pass

        # This for-loop searches for the variables in the event created
        # by the parser and writes them into arrays which will be used
        # to create the graphs later.
        for i, variablePair in enumerate(self._variablePairs):
            # the variablePair now contains the pair of variables which we got
            # from the option in beginJob, lets write them in separate variables
            # for x and y:
            xVariableName, yVariableName = variablePair
            # Now we do the same with the errors. Since we loop over the variable-
            # pairs and count ("enumerate") we can access with the counter "i":
            xErrorName, yErrorName = self._errorPairs[i]
            # Same again for units:
            xUnit, yUnit = self._unitPairs[i]

            # Initialize the actual string of variables which will be appended to the
            # axis titles
            xUnitString = ""
            yUnitString = ""
            # If we have a unit which is not 0 save it in the string with brackets 
            # around it
            if xUnit != 0:
                xUnitString = " (" + xUnit + ")"
            if yUnit != 0:
                yUnitString = " (" + yUnit + ")"

            # Initialize arrays for values in x and y and also for the errors:
            xValues = array('f', [])
            xErrors = array('f', [])
            yValues = array('f', [])
            yErrors = array('f', [])
            foundX = False
            foundY = False
            foundEX = False
            foundEY = False

            # For the TGraph and TGraphErrors constructors we need the 
            # number of entries in the arrays, saved in this variable
            # (could also exchange nEntries with len(xValues) in the
            # constructor)
            nEntries = 0
            # Loop over all entries in the event from the txt file...
            # A variableSet now contains all entries in the txt file
            # for one variable, e.g. x
            for variableSet in container.getObjects():
                # This gets either the title from the txt file or the
                # automatically created title "value"+entryNumber
                currentVariableName = variableSet.getName()
                # If the variable set is the one corresponding to the 
                # x variable we want to plot
                if currentVariableName == xVariableName:
                    # fill it into the x-array
                    for i in range(0, variableSet.getSize()):
                        xValues.append(variableSet.getElement(i))
                        foundX = True
                        # if there is no error given the error-array has 
                        # to be of same size, so fill it here:
                        if (xErrorName == 0):
                            xErrors.append(0)
                            foundEX = True
                    # need this only for graph constructor
                    nEntries = variableSet.getSize()
                # If the variable set is the one corresponding to the
                # error of the x variable
                if currentVariableName == xErrorName:
                    foundEX = True
                    # fill into x-error-array...
                    for i in range(0, variableSet.getSize()):
                        xErrors.append(variableSet.getElement(i))

                # If the variable set is the one corresponding to the
                # y variable
                if currentVariableName == yVariableName:
                    foundY = True
                    # fill into y-array...
                    for i in range(0, variableSet.getSize()):
                        yValues.append(variableSet.getElement(i))
                        # if there is no error given the error-array has 
                        # to be of same size, so fill it here:
                        if (yErrorName == 0):
                            foundEY = True
                            yErrors.append(0)

                # If the variable set is the one corresponding to the
                # error of the y variable
                if currentVariableName == yErrorName:
                    foundEY = True
                    # fill into y-error-array...
                    for i in range(0, variableSet.getSize()):
                        yErrors.append(variableSet.getElement(i))

            # Catch if the variables specified are not in the file...
            if not foundX or not foundY or not foundEX or not foundEY:
                errorString = "ERROR in GraphPlotter: Sorry, could not find all variables specified make sure they are in the text file:"
                if not foundEX: errorString += " could not find '" + xErrorName + "'"
                if not foundEY: errorString += " could not find '" + yErrorName + "'"
                if not foundX: errorString += " could not find '" + xVariableName + "'"
                if not foundY: errorString += " could not find '" + yVariableName + "'"
                raise NameError(errorString)

### USER HOOK ----- Here you can customize your plots, changes in this part
#                   are more or less save
#
            # This initializes a new graph and takes care of the looks 
            # of it, can be customized with normal pyRoot syntax later.
            # For color codes see: http://root.cern.ch/root/html/MACRO_TColor_1_c.gif
            # For marker codes see: http://root.cern.ch/root/html/MACRO_TAttMarker_3_c.gif
            graph = TGraphErrors(nEntries, xValues, yValues, xErrors, yErrors)

            self.initGraphStyle(graph=graph, xTitle=xVariableName + xUnitString, yTitle=yVariableName + yUnitString,
                graphTitle="", markerColor=2, marker=20, lineWidth=1, lineColor=2, lineStyle=1)
            # if you want to change the looks of a graph for a certain variable pair you can uncomment (delete the 
            # '#') the following if statement and edit the arguments in the initGraphStyle accordingly:
            # NOTE that if you want to change for example the line-style you'll have to change it in the graph.Draw
            # argument below as well
            #if xVariableName = "put your xVariableName here" and yVariableName == "put your yVariableName here":
            #    self.initGraphStyle(graph=graph, xTitle = xVariableName+xUnitString, yTitle = yVariableName+yUnitString, 
            #    graphTitle = "", markerColor = 2, marker = 20, lineWidth = 1, lineColor = 2, lineStyle = 1)

            c1 = TCanvas()
            # IMPORTANT NOTE: The character-chain you give the Draw-function
            # specifies what will be drawn, this is unrelated to the options 
            # in the initialization above. Common options are:
            # "A"	Axis are drawn around the graph
            # "L"	A simple polyline between every points is drawn
            # "C"	A smooth Curve is drawn
            # "*"	A Star is plotted at each point
            # "P"	Idem with the current marker
            # for more check: http://root.cern.ch/root/html/TGraphPainter.html

            # start user code to measure conductivity ------------

            graph.GetXaxis().SetLimits(0,7)
            graph.SetMinimum(0)
            graph.SetMaximum(80)

            lin_pol=TF1("lin_pol","[0]+[1]*x",0,10)
            graph.Fit("lin_pol")

            # end user code --------------------------------------

            graph.Draw("AP")

            self._saveAs = self._saveAs.format(filenumber=fileCounter)
            if (self._saveAs.find("\\") == -1 and  self._saveAs.find("/") == -1):
                self._saveAs = self._moduleRef.getAnalysis().getOutputPath() + self._saveAs
            c1.Print(self._saveAs)

            fileCounter += 1

            # If you are annoyed by all the pop-up windows containing graphs 
            # delete this line or better still: put comment in front.
            #c1.WaitPrimitive()

### USER HOOK ----- End of analyse, after this only helper functions...

    def endJob(self):
        '''Executed after the last object'''
        print '*** End job: GraphPlotter'

    def initGraphStyle(self, graph, xTitle, yTitle, graphTitle, markerColor=1, marker=20, lineWidth=0, lineColor=1, lineStyle=1):
        # Use the standard style...
        graph.UseCurrentStyle()
        # Self explainatory...
        graph.SetLineWidth(lineWidth)
        graph.SetLineColor(lineColor)
        graph.SetMarkerColor(markerColor)
        graph.SetMarkerStyle(marker)
        graph.GetXaxis().SetTitle(xTitle)
        graph.GetYaxis().SetTitle(yTitle)
        graph.SetTitle(graphTitle)
        graph.SetLineStyle(lineStyle)

    def setupRoot(self):
        gROOT.Reset()   # reset global variables

        # Set ROOT style...
        # If you want to change this check out the
        # Root-manual
        gROOT.SetStyle("Plain")
        gStyle.SetOptStat(0)
        gStyle.SetOptFit(11)
        gStyle.SetStatX(0.9)
        gStyle.SetStatY(0.9)
        gStyle.SetStatW(0.3)
        gStyle.SetStatH(0.2)
        gStyle.SetNdivisions(505, "X")
        gStyle.SetNdivisions(505, "Y")
        gStyle.SetLabelOffset(0.008, "X")
        gStyle.SetLabelSize(0.08, "X")
        gStyle.SetTitleSize(0.08, "X")
        gStyle.SetTitleOffset(1.0, "X")
        gStyle.SetLabelOffset(0.02, "Y")
        gStyle.SetLabelSize(0.08, "Y")
        gStyle.SetTitleSize(0.08, "Y")
        gStyle.SetTitleOffset(1.4, "Y")
        gStyle.SetPadTopMargin(0.15)
        gStyle.SetPadBottomMargin(0.2)
        gStyle.SetPadLeftMargin(0.22)
        gStyle.SetPadRightMargin(0.1)
        gStyle.SetLineWidth(1)
        gStyle.SetMarkerStyle(20)
        gStyle.SetMarkerSize(0.9)
        gStyle.SetMarkerColor(2)
        gStyle.SetCanvasColor(10)
        gStyle.SetFrameFillColor(10)

    def splitPairOption(self, rawPairs, option):
        splittedPairOption = []
        if len(rawPairs) != 0:
            for item in rawPairs:
                if len(item.split(":")) != 2:
                    print "ERROR in GraphPlotter: Please always give pairs in", option, "as in 'x:y t:x'. Do not use quotes."
                    self._optionsGood = False
                else:
                    xVal = item.split(":")[0]
                    yVal = item.split(":")[1]
                    if xVal == "0": xVal = float(xVal)
                    if yVal == "0": yVal = float(yVal)
                    splittedPairOption.append((xVal, yVal))

        # If less units than variables are given fill with 0s...
        if len(splittedPairOption) < len(self._variablePairs):
            print "WARNING: Please give as many", option, "as variablePairs. Filling the rest with 0s."
            nDefPairs = len(splittedPairOption)
            for i in range(len(self._variablePairs) - nDefPairs):
                splittedPairOption.append((0, 0))

        return splittedPairOption

