#
# Purpose: histogram final state particle kinematics
#
# Author : Martin Erdmann
# Date   : 5-Jul-2008
# Modified: Martin Erdmann
# Date    : 9-Apr-2009
# access to ROOT classes
#

from ROOT import gROOT, gStyle, TH1F, TCanvas

from pxl import modules

try:
    from pxl import root
    rootEnabled = True
except:
    print "Using PXL version < 3.3 or PXL compiled without ROOT"
    rootEnabled = False

class Histogram(modules.PythonModule):
    def initialize(self, module):
        self.__module = module
    
    #----------------------------------------------------------------------
    def beginJob(self, parameters=None):
        # ROOT histogram style
        gROOT.Reset()
        gROOT.SetStyle("Plain")
        
        if rootEnabled:
            root.PxlStyle.initRootPlotStyle()

        print "*** start filling histograms ***"

        # particle histograms
        self.h_pt = TH1F ("h_pt" , "particle transverse momentum", 40, 0, 200)
        self.h_eta = TH1F ("h_eta", "particle pseudo rapidity"    , 30, - 3, 3)
        
        # histogram specifications

        self.h_pt.UseCurrentStyle()
        self.h_eta.UseCurrentStyle()
        self.h_pt .GetXaxis().SetTitle("pt [GeV]")
        self.h_eta.GetXaxis().SetTitle("\\eta")
        self.h_pt .GetYaxis().SetTitle("N")
        self.h_eta.GetYaxis().SetTitle("N")

    #----------------------------------------------------------------------
    def analyse(self, object):
        # only process pxl::Events
        event = core.toEvent(object)
        if not event:
            return
        
        # access to the event views of the event
        eventviews = event.getEventViews()

        # plot reconstructed final state particles
        for eventview in eventviews:
            if eventview.getName() == "Reconstructed":
                particles = eventview.getParticles()
                for particle in particles:
                    if particle.getDaughterRelations().size() == 0 and particle.getName() != "MET":
                        self.h_pt.Fill (particle.getPt())
                        self.h_eta.Fill(particle.getEta())

#----------------------------------------------------------------------
    def endJob(self):
        c1 = TCanvas('particle kinematics')
        c1.Divide(2, 1)
        c1.cd(1)
        self.h_pt.Draw("e")
        c1.cd(2)
        self.h_eta.Draw("e")
        c1.Print(self.__module.getAnalysis().getOutputPath() + "output/histogram_example_output.pdf")

        print "*** histogram filling finished. ***"
    
        print " Closing all ROOT Canvases will finish the analysis"
        c1.WaitPrimitive()
