#
# Purpose: Fill selected variables of selected particles in
#          selected eventviews into a ntuple
#
# Author : Andreas Hinzmann
# Date   : 17-Jul-2008
#

from ROOT import gROOT, TFile, TNtuple
from array import array

from pxl import modules

class NTupleFiller(modules.PythonModule):
    def initialize(self, module):
        self._module = module
        self._module.addStringOption("rootfilename","rootfilename","ntuple.root")
        self._module.addStringOption("event_userrecord_names","event_userrecord_names","")
        self._module.addStringOption("eventview_names","eventview_names","Reconstructed")
        self._module.addStringOption("eventview_userrecord_names","eventview_userrecord_names","")
        self._module.addStringOption("particle_names","particle_names","Muon_1 Muon_2")
        self._module.addStringOption("variable_names","variable_names","pt eta phi mass")
    
    #----------------------------------------------------------------------
    def beginJob(self, parameters=None):

        print "*** reading parameters"
        
        self.rootfilename = ""
        self.hfile = 0
        self.event_userrecord_names=[]
        self.eventview_names = []
        self.eventview_userrecord_names=[]
        self.particle_names = []
        self.variable_names = []
        self.entry_names = []
        self.ntuple = 0
        self.ntuple_list = []
        self.count_events = 0

        self.rootfilename = self._module.getAnalysis().getOutputPath() + self._module.getStringOption("rootfilename")
        self.event_userrecord_names = self._module.getStringOption('event_userrecord_names').split()
        self.eventview_names = self._module.getStringOption('eventview_names').split()
        self.eventview_userrecord_names = self._module.getStringOption('eventview_userrecord_names').split()
        self.particle_names = self._module.getStringOption('particle_names').split()
        self.variable_names = self._module.getStringOption('variable_names').split()

        print "rootfilename = " , self.rootfilename
        print "event_userrecord_names = " , self.event_userrecord_names
        print "eventview_names = " , self.eventview_names
        print "eventview_userrecord_names = " , self.eventview_userrecord_names
        print "particle_names = " , self.particle_names
        print "variable_names = " , self.variable_names

        # initialize ROOT
        gROOT.Reset()

        print "*** opening file"

        self.hfile = TFile (self.rootfilename, "RECREATE", "Ntuple filler")

        print "*** booking ntuple"

        # book ntuple for all variables of all particles in all eventviews
        self.entry_names = ["event"]
        self.ntuple_content = "event"
        self.ntuple_list = [0]
        for userrecord_name in self.event_userrecord_names:
            self.entry_names.append(userrecord_name)
            self.ntuple_content += ":" + userrecord_name
            self.ntuple_list.extend([0])
        for eventview_name in self.eventview_names:
            for userrecord_name in self.eventview_userrecord_names:
                entry_name = eventview_name + "_" + userrecord_name
                self.entry_names.append(entry_name)
                self.ntuple_content += ":" + entry_name
                self.ntuple_list.extend([0])
            for particle_name in self.particle_names:
                for variable_name in self.variable_names:
                    entry_name = eventview_name + "_" + particle_name + "_" + variable_name
                    self.entry_names.append(entry_name)
                    self.ntuple_content += ":" + entry_name
                    self.ntuple_list.extend([0])
        self.ntuple = TNtuple ("ntuple", "ntupledump", self.ntuple_content)

    #----------------------------------------------------------------------
    def analyse(self, object):
        # only process pxl::Events
        event = core.toEvent(object)
        if not event:
            return
        
        self.count_events += 1

        for i in range(len(self.ntuple_list)):
            self.ntuple_list[i] = - 100000
        self.ntuple_list[0] = self.count_events
        i=1
        for userrecord_name in self.event_userrecord_names:
            self.ntuple_list[i] = event.findUserRecord(userrecord_name, -100000) 
            i+=1

        # loop over all eventviews
        eventviews = event.getEventViews()
        for eventview in eventviews:
            thiseventview_name = eventview.getName()
            if thiseventview_name in self.eventview_names:
                for userrecord_name in self.eventview_userrecord_names:
                    entry_name = thiseventview_name + "_" + userrecord_name
                    entry_i = self.entry_names.index(entry_name)
                    self.ntuple_list[entry_i] = eventview.findUserRecord(userrecord_name, -100000) 

                # count Jets, Muons, Electrons,... and name Jet1, Jet2, Jet3,...
                counters = {}

                particles = eventview.getParticles()
                for particle in particles:
                    thisparticle_name = particle.getName()
                    if not thisparticle_name in counters.keys():
                        counters[thisparticle_name] = 0
                    counters[thisparticle_name] += 1
                    thisparticle_name += "_%d" % (counters[thisparticle_name],)
                    if thisparticle_name in self.particle_names:
                        for variable_name in self.variable_names:
                            entry_name = thiseventview_name + "_" + thisparticle_name + "_" + variable_name
                            print "Filling",entry_name
                            entry_i = self.entry_names.index(entry_name)

                            # fill ntuple entry for selected variable of selected particle
                            if variable_name == "px":
                                self.ntuple_list[entry_i] = particle.getPx()
                            if variable_name == "py":
                                self.ntuple_list[entry_i] = particle.getPy()
                            if variable_name == "pz":
                                self.ntuple_list[entry_i] = particle.getPz()
                            if variable_name == "E":
                                self.ntuple_list[entry_i] = particle.getE()
                            if variable_name == "pt":
                                self.ntuple_list[entry_i] = particle.getPt()
                            if variable_name == "eta":
                                self.ntuple_list[entry_i] = particle.getEta()
                            if variable_name == "phi":
                                self.ntuple_list[entry_i] = particle.getPhi()
                            if variable_name == "mass":
                                self.ntuple_list[entry_i] = particle.getMass()
                            if variable_name == "charge":
                                self.ntuple_list[entry_i] = particle.getCharge()
                            if variable_name == "id":
                                self.ntuple_list[entry_i] = particle.getParticleId()

        # fill ntuple
        ntuple_fill = array('f', [])
        ntuple_fill.fromlist(self.ntuple_list)
        self.ntuple.Fill (ntuple_fill)

    #----------------------------------------------------------------------
    def endJob(self):
        print "*** writing file",self.rootfilename
        self.hfile.Write();
        self.hfile.Close();

        print "*** done"
