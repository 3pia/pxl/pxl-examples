### Skeleton for plot_cosmicrays.py
# created by VISPA
# Tue Jul 10 15:51:41 2012
### PyAnalyse skeleton script

from pxl import astro, core, healpix, modules
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
from numpy import argsort, log10, zeros
# to make sure the script doesn't crash if there are Healpix regions in the container
healpix.Healpix_initialize()

class Example(modules.PythonModule):
    '''This Module creates a plot of the cosmic rays contained in a BasicContainer 
    for each container passed through the module. The cosmic rays are plotted in a hammer
    projection with higher energetic cosmic rays shown in foreground.'''
    def __init__(self):
        ''' Initialize private variables '''
        modules.PythonModule.__init__(self)
        #self._exampleVariable = startValue

    def initialize(self, module):
        ''' Initialize module options '''
        module.addOption("SaveToFigure", "Choose filename for the figure. In case of more than one universe in the basic container, a counter is attached to the filename", "cr_dist.png")
        self.__module = module

    def beginJob(self, parameters=None):
        '''Executed before the first object comes in'''
        print '*** Begin job'
        self.__counter = 0
        self.__saveToFigure = self.__module.getOption("SaveToFigure")

    def beginRun(self):
        '''Executed before each run'''
        pass

    def analyse(self, object):
        '''Executed on every object'''

        # open data container
        bc = core.toBasicContainer(object)
        self.__counter += 1

        fig = plt.figure()
        fig.add_subplot(111, projection='hammer')

        # retrieve list of cosmic ray objects from container
        crs = bc.getObjectsOfType(astro.UHECR)

        lons = zeros(len(crs))
        lats = zeros(len(crs))
        energies = zeros(len(crs))

        for i, cr in enumerate(crs):
            lons[i] = cr.getGalacticLongitude()
            lats[i] = cr.getGalacticLatitude()
            #energies given in EeV, we want to show energies logarithmically
            energies[i] = 18+log10(cr.getEnergy())

        # plot highest energies on top
        indices = argsort(energies)
        lons = lons[indices]
        lats = lats[indices]
        energies = energies[indices]

        plt.scatter(lons, lats, c=energies, vmin=18.5, vmax=20.5)
        plt.grid()

        plt.text(0.5,1.8, "UHECR arrival distribution", horizontalalignment='center')
        co = plt.colorbar(orientation='horizontal')
        co.set_label("Energy [log(E/eV)]")


        filename, delimiter, ending = self.__saveToFigure.rpartition('.')
        fig.savefig(filename + str(self.__counter) + delimiter + ending)
    def endRun(self):
        '''Executed after each run'''
        pass

    def endJob(self):
        '''Executed after the last object'''
        print '*** End job'
