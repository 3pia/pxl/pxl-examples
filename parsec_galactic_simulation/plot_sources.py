### Skeleton for plot_sources.py
# created by VISPA
# Tue Jul 10 15:51:41 2012
### PyAnalyse skeleton script

from pxl import astro, core, healpix, modules
import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt
from numpy import argsort, log10, zeros
# to make sure the script doesn't crash if there are Healpix regions in the container
healpix.Healpix_initialize()

class Example(modules.PythonModule):
    '''This module creates a plot of the UHECR sources contained in a Basic Container. The 
    size of the sources in the plot is scaled anti proportional to their distance to the 
    observer.'''
    def __init__(self):
        ''' Initialize private variables '''
        modules.PythonModule.__init__(self)
        #self._exampleVariable = startValue

    def initialize(self, module):
        ''' Initialize module options '''
        module.addOption("SaveToFigure", "Choose filename for the figure. In case of more than one universe in the basic container, a counter is attached to the filename", "cr_dist.png")
        self.__module = module

    def beginJob(self, parameters=None):
        '''Executed before the first object comes in'''
        print '*** Begin job'
        self.__counter = 0
        self.__saveToFigure = self.__module.getOption("SaveToFigure")

    def beginRun(self):
        '''Executed before each run'''
        pass

    def analyse(self, object):
        '''Executed on every object'''

        # open data container
        bc = core.toBasicContainer(object)
        self.__counter += 1

        fig = plt.figure()
        fig.add_subplot(111, projection='hammer')

        # retrieve list of source objects
        srcs = bc.getObjectsOfType(astro.UHECRSource)
        for s in srcs:
            d = s.getDistance()
            plt.plot(s.getGalacticLongitude(), s.getGalacticLatitude(), 'g+',
                markersize=(1000.-d)/20.+5., markeredgewidth=2.)

        plt.grid()

        plt.text(0.5,1.8, "Source distribution", horizontalalignment='center')

        filename, delimiter, ending = self.__saveToFigure.rpartition('.')
        fig.savefig(filename + str(self.__counter) + delimiter + ending)
    def endRun(self):
        '''Executed after each run'''
        pass

    def endJob(self):
        '''Executed after the last object'''
        print '*** End job'
