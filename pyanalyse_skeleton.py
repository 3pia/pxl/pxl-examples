### Skeleton for pyanalyse_skeleton.py
# created by VISPA
# Wed Jul 11 18:58:42 2012
### PyAnalyse skeleton script

from pxl import modules

class Example(modules.PythonModule):
    def __init__(self):
        ''' Initialize private variables '''
        modules.PythonModule.__init__(self)
        #self._exampleVariable = startValue

    def initialize(self, module):
        ''' Initialize module options '''
        #module.addOption("example option", "example description", "default value")

    def beginJob(self, parameters=None):
        '''Executed before the first object comes in'''
        print '*** Begin job'

    def beginRun(self):
        '''Executed before each run'''
        pass

    def analyse(self, object):
        '''Executed on every object'''
        #event=core.toEvent(object)

    def endRun(self):
        '''Executed after each run'''
        pass

    def endJob(self):
        '''Executed after the last object'''
        print '*** End job'
