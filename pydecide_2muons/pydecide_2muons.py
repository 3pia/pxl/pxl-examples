### Skeleton for decidetest.py
# created by VISPA
# Tue Jul 10 12:39:22 2012
### PyDecide skeleton script

from pxl import astro, core, healpix, modules
healpix.Healpix_initialize()

class Example(modules.PythonModule):
    '''This module selects all events containing two reconstructed Muons'''
    def __init__(self):
        ''' Initialize private variables '''
        modules.PythonModule.__init__(self)
        #self._exampleVariable = startValue

    def initialize(self, module):
        ''' Initialize module sinks, sources and options '''
        pass

    def beginJob(self, parameters=None):
        '''Executed before the first object comes in'''
        print '*** Begin job'

    def beginRun(self):
        '''Executed before each run'''
        pass

    def decide(self, object):
        '''Executed on every object'''
        bc = core.toEvent(object)

        passIt = False

        for eve in bc.getEventViews():
            muonCounter = 0
            if eve.getName() == 'Reconstructed':
                for particle in eve.getParticles():
                    if particle.getName() == 'Muon':
                        muonCounter += 1
        if muonCounter == 2 :
            passIt = True

        return passIt
        # return True or False

    def endRun(self):
        '''Executed after each run'''
        pass

    def endJob(self):
        '''Executed after the last object'''
