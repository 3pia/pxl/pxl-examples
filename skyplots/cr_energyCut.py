### Skeleton for energyCut.py
# created by VISPA
# Mon May 21 14:32:30 2012
### PyAnalyse skeleton script

from pxl import modules, core, astro, healpix
healpix.Healpix_initialize()

class Example(modules.PythonModule):
    def __init__(self):
        ''' Initialize private variables '''
        modules.PythonModule.__init__(self)
        self.__emin = 0

    def initialize(self, module):
        ''' Initialize module options '''
        module.addOption("EnergyCut", "Select minimum energy for UHECRs [EeV]",
                         3.0)
        self.__module = module

    def beginJob(self, parameters=None):
        '''Executed before the first object comes in'''
        print '*** Begin job'
        self.__emin = self.__module.getOption("EnergyCut")

    def beginRun(self):
        '''Executed before each run'''
        self.__counter = 0

    def analyse(self, object):
        '''Executed on every object'''
        bc = core.toBasicContainer(object)
        crs = bc.getObjectsOfType(astro.UHECR)
        for cr in crs:
            if cr.getEnergy() < self.__emin:
                bc.remove(cr)
            else:
                self.__counter += 1

    def endRun(self):
        '''Executed after each run'''
        print "Kept %d cosmic rays with energy higher than %.1f EeV."%(self.__counter, 
                                                                  self.__emin)

    def endJob(self):
        '''Executed after the last object'''
        print '*** End job'
