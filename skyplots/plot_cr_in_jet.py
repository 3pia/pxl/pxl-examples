### Skeleton for plot_cosmicrays.py
# created by VISPA
# Tue Jul 10 15:51:41 2012
### PyAnalyse skeleton script

from pxl import astro, core, healpix, modules
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
from numpy import argsort, array, linspace, log10, pi, zeros
# to make sure the script doesn't crash if there are Healpix regions in the container
healpix.Healpix_initialize()

class Example(modules.PythonModule):
    '''This module plots all cosmic rays that are associated through a SoftRelation to a jet 
    or region of interest. The ROI can be specified by the name of the SoftRelation, which is 
    by default "cr_in_jet" for ROIs generated with the SISCone module.'''

    def __init__(self):
        ''' Initialize private variables '''
        modules.PythonModule.__init__(self)
        #self._exampleVariable = startValue

    def plotAugerCoverageBoundary(self, dalpha = 0, lw=2):
        a = astro.AstroObject()
        ra = linspace(0,2*pi,100)
        lat = zeros(len(ra))
        lon = zeros(len(ra))
        for i,r in enumerate(ra):
            a.setEquatorialCoordinates(r,24.75/180*pi-dalpha)
            lat[i] = a.getGalacticLatitude()
            lon[i] = a.getGalacticLongitude()
        i = argsort(lon)
        plt.plot(lon[i],lat[i],'k--', lw=lw)

    def initialize(self, module):
        ''' Initialize module options '''
        module.addOption("SaveToFigure", "Choose filename for the figure. In case of more than one universe in the basic container, a counter is attached to the filename", "cr_in_jets.png")
        module.addOption("JetIdentifier", "If a CR belongs to a region of interest, it is identified with this identifier", "cr_in_jet")
        self.__module = module

    def beginJob(self, parameters=None):
        '''Executed before the first object comes in'''
        print '*** Begin job'
        self.__counter = 0
        self.__saveToFigure = self.__module.getOption("SaveToFigure")
        self.__jetIdentifier = self.__module.getOption("JetIdentifier")

    def beginRun(self):
        '''Executed before each run'''
        pass

    def analyse(self, object):
        '''Executed on every object'''
        bc = core.toBasicContainer(object)
        self.__counter += 1

        fig = plt.figure()
        fig.add_subplot(111, projection='hammer')

        crs = bc.getObjectsOfType(astro.UHECR)

        lons = []
        lats = []
        energies = []

        for cr in crs:
            for key, item in cr.getSoftRelations().getContainer().items():
                if key == self.__jetIdentifier:
                    lons.append(cr.getGalacticLongitude())
                    lats.append(cr.getGalacticLatitude())
                    energies.append(18+log10(cr.getEnergy()))
                    break
        
        lons = array(lons)
        lats = array(lats)
        energies = array(energies)

        indices = argsort(energies)
        lons = lons[indices]
        lats = lats[indices]
        energies = energies[indices]

        plt.scatter(lons, lats, c=energies, vmin=18.5, vmax=20.5)
        plt.grid()
        self.plotAugerCoverageBoundary()

        plt.text(0.5,1.8, "UHECR in SISCone jets", horizontalalignment='center')
        co = plt.colorbar(orientation='horizontal')
        co.set_label("Energy [log(E/eV)]")


        filename, delimiter, ending = self.__saveToFigure.rpartition('.')
        fig.savefig(filename + str(self.__counter) + delimiter + ending)
    def endRun(self):
        '''Executed after each run'''
        pass

    def endJob(self):
        '''Executed after the last object'''
        print '*** End job'
