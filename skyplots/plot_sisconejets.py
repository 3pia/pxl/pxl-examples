### Skeleton for plot_cosmicrays.py
# created by VISPA
# Tue Jul 10 15:51:41 2012
### PyAnalyse skeleton script

from pxl import astro, core, healpix, modules
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
from numpy import argsort, linspace, log10, pi, zeros
# to make sure the script doesn't crash if there are Healpix regions in the container
healpix.Healpix_initialize()

class Example(modules.PythonModule):
    '''This module plots the individual regions of interest created with the SISCone jet module.
    It is similar to the plot_cr_in_jet module, but plots the cosmic rays belonging to one 
    region of interest in the same color to indicate the shape of the regions.'''
    def __init__(self):
        ''' Initialize private variables '''
        modules.PythonModule.__init__(self)
        #self._exampleVariable = startValue

    def plotAugerCoverageBoundary(self, dalpha = 0, lw=2):
        a = astro.AstroObject()
        ra = linspace(0,2*pi,100)
        lat = zeros(len(ra))
        lon = zeros(len(ra))
        for i,r in enumerate(ra):
            a.setEquatorialCoordinates(r,24.75/180*pi-dalpha)
            lat[i] = a.getGalacticLatitude()
            lon[i] = a.getGalacticLongitude()
        i = argsort(lon)
        plt.plot(lon[i],lat[i],'k--', lw=lw)

    def initialize(self, module):
        ''' Initialize module options '''
        module.addOption("SaveToFigure", "Choose filename for the figure. In case of more than one universe in the basic container, a counter is attached to the filename", "cr_siscone_jets.png")
        self.__module = module

    def beginJob(self, parameters=None):
        '''Executed before the first object comes in'''
        print '*** Begin job'
        self.__counter = 0
        self.__saveToFigure = self.__module.getOption("SaveToFigure")

    def beginRun(self):
        '''Executed before each run'''
        pass

    def analyse(self, object):
        '''Executed on every object'''
        bc = core.toBasicContainer(object)
        self.__counter += 1

        fig = plt.figure()
        fig.add_subplot(111, projection='hammer')

        colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k',
                       'BlueViolet', 'Brown','CadetBlue','Chartreuse',
                       'CornflowerBlue','Crimson','DarkGoldenRod',
                       'DarkOliveGreen','Darkorange','DarkOrchid',
                       'DarkSalmon','GreenYellow','MediumBlue',
                       'LightSteelBlue','RoyalBlue','Peru',
                       'Plum','Purple','RosyBrown','SandyBrown']



        rois = bc.getObjectsOfType(astro.RegionOfInterest)
        crs = bc.getObjectsOfType(astro.UHECR)
        for i, roi in enumerate(rois):
            plt.plot(roi.getGalacticLongitude(), roi.getGalacticLatitude(), '*',
                markersize=3.5)

            lons = []
            lats = []
            for key, item in roi.getSoftRelations().getContainer().items():
                uhecr = astro.toUHECR(bc.getById(item))
                if isinstance(uhecr, astro.UHECR):
                    #plot(uhecr.getGalacticLongitude(), uhecr.getGalacticLatitude(),
                    #     '%s.'%colors[i%8], markersize=3)
                    lats.append(uhecr.getGalacticLatitude())
                    lons.append(uhecr.getGalacticLongitude())

            plt.scatter(lons, lats, color=colors[i%len(colors)])

        plt.grid()
        plt.text(0.5,1.8, "SISCone jets", horizontalalignment='center')
        self.plotAugerCoverageBoundary()

        filename, delimiter, ending = self.__saveToFigure.rpartition('.')
        fig.savefig(filename + str(self.__counter) + delimiter + ending)
    def endRun(self):
        '''Executed after each run'''
        pass

    def endJob(self):
        '''Executed after the last object'''
        print '*** End job'
