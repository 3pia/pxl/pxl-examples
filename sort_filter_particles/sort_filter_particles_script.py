#
# Purpose: sort reconstructed particles by pt,
#          filter particles with |eta| < 2.0
#
# Author : Andreas Hinzmann
# Date   : 18-June-2009
#
# Modified : Robert Fischer
# Date     : 23-Jun-2010
#
import os.path
from pxl import modules

# Purpose: Filter particles with |eta|<2.0 and create a second EventView with filtered objects,
# sorted by pT
class SortFilter(modules.PythonModule):
    #----------------------------------------------------------------------
    def initialize(self, module):
        pass
    
    #----------------------------------------------------------------------
    def beginJob (self, parameters=None):
        print "*** sort and filter particles"
    
    #----------------------------------------------------------------------
    def analyse (self, object):
        # only process pxl::Events
        event = core.toEvent(object)
        if not event:
            return
    
        # Create filtered EventView
        filtered_eventview = hep.EventView()
        filtered_eventview.setName("Filtered")
        # Set owner to event
        event.setObject(filtered_eventview)
        
        # Alternatively, directly let event create new event view:
        #    filtered_eventview = event.createEventView()
        # The event is automatically the owner of the event view
        
        eventviews = event.getEventViews()
        for eventview in eventviews:
            if eventview.getName()=="Reconstructed":
                # use ParticleFilter to order Particles by Pt
                ptFilter=hep.ParticleFilter()
                # Book filter criterion: Minimum pt = 0, ignore name, maximum |eta| = 2.0
                criterion = hep.ParticlePtEtaNameCriterion("", 0, 2.0)
                # Create particle vector for sorted particles
                sortedParticles = hep.ParticleVector()
                # Apply particle filter with pt, eta, name criterion
                ptFilter.apply(eventview.getObjectOwner(), sortedParticles, criterion)
                # save a copy of the filtered particles in the eventview "Filtered"
                for i in range(len(sortedParticles)):
                    print "Filter selected", sortedParticles[i]
                    filtered_eventview.setObject(sortedParticles[i].copy())
    
    #----------------------------------------------------------------------
    def endJob(self):
        print "*** finish sorting"
