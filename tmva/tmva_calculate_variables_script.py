from math import fmod,fabs,pi

class Calculate:
    def __init__(self):
        pass

    #----------------------------------------------------------------------
    def analyse(self, object):
        '''Executed on every object'''
        # only process pxl::Events
        event = core.toEvent(object)
        if not event:
            return

        # access to the event views of the event
        eventviews = event.getEventViews()
        for eventview in eventviews:
            if eventview.getName() == "Reconstructed":
                particles = eventview.getParticles()
                countJets=0
                allJets=[]
                for particle in particles:
                    if particle.getName() == "Jet":
                        countJets+=1
                        allJets+=[particle]
                        # store single jet variables in event
                        event.setUserRecord('jet'+str(countJets)+'_pt',particle.getPt())
                        event.setUserRecord('jet'+str(countJets)+'_eta',particle.getEta())
                        event.setUserRecord('jet'+str(countJets)+'_phi',particle.getPhi())
                # store global variables in event
                sumParticle=hep.Particle()
                sumParticle+=allJets[0]
                sumParticle+=allJets[1]
                event.setUserRecord('sum_jets_mass',sumParticle.getMass())
                event.setUserRecord('delta_jets_eta',fabs(allJets[0].getEta()-allJets[1].getEta()))
